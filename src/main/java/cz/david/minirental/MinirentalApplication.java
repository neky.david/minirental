package cz.david.minirental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class MinirentalApplication {

	public static final String AJAX_HEADER_NAME = "X-Requested-With";
	public static final String AJAX_HEADER_VALUE = "XMLHttpRequest";

	public static void main(String[] args) {
		SpringApplication.run(MinirentalApplication.class, args);
	}

}
