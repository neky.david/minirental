package cz.david.minirental.specifications;

import cz.david.minirental.model.entity.Borrow;
import org.springframework.data.jpa.domain.Specification;

import java.util.Calendar;

public class BaseSpecification {

    public static <T> Specification<T> isInRange(Calendar from, Calendar to, String key) {
        return isInRange(from, to, key, key);
    }

    public static <T> Specification<T> isInRange(Calendar from, Calendar to, String key1, String key2) {
        return (Specification<T>) (root, criteria, cb) -> {
            if (from != null && to != null) {
                return cb.and(cb.greaterThanOrEqualTo(root.get(key1), from),
                        cb.lessThanOrEqualTo(root.get(key2), to));
            } else {
                return null;
            }
        };
    }

    public static <T> Specification<T> isActive(Boolean active) {
        return (Specification<T>) (root, criteria, cb) -> {
            if (active != null) {
                return cb.equal(root.get("active"), active);
            } else {
                return null;
            }
        };
    }

}
