package cz.david.minirental.specifications;

import cz.david.minirental.model.entity.Role;
import cz.david.minirental.model.entity.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.Collection;

public class UserSpecification {

    public static Specification<User> nameLike(String name) {
        return (Specification<User>) (root, criteria, cb) -> {
            if (name != null && !name.isEmpty()) {
                return cb.or(cb.like(cb.lower(root.get("name")), name),
                        cb.like(cb.lower(root.get("surname")), name));
            } else {
                return null;
            }
        };
    }

    public static Specification<User> roleEqual(String roleName) {
        return (Specification<User>) (root, query, cb) -> {
            if (roleName != null && !roleName.isEmpty()) {
                /*query.distinct(true);
                Root<Role> role = query.from(Role.class);
                Expression<Collection<User>> roleUsers = root.get("roles");
                return cb.and(cb.equal(role.get("name"), roleName), cb.isMember(root, roleUsers));*/

                return cb.equal(root.join("roles", JoinType.INNER).get("name"), roleName);
            } else {
                return null;
            }
        };
    }

    public static Specification<User> phoneNumberLike(String phone) {
        return (Specification<User>) (root, criteria, cb) -> {
            if (phone != null && !phone.isEmpty()) {
                return cb.like(cb.lower(root.get("phoneNumber")), phone);
            } else {
                return null;
            }
        };
    }

    public static Specification<User> addressLike(String address) {
        return (Specification<User>) (root, criteria, cb) -> {
            if (address != null && !address.isEmpty()) {
                return cb.or(
                        cb.or(
                                cb.like(cb.lower(root.get("address").get("street")), address),
                                cb.like(cb.lower(root.get("address").get("city")), address))
                        ,cb.or(
                                cb.like(cb.lower(root.get("address").get("zip")), address),
                                cb.like(cb.lower(root.get("address").get("state")), address)
                        ));
            } else {
                return null;
            }
        };
    }

    public static Specification<User> companyLike(String company) {
        return (Specification<User>) (root, criteria, cb) -> {
            if (company != null && !company.isEmpty()) {
                return cb.like(cb.lower(root.get("company")), company);
            } else {
                return null;
            }
        };
    }

    public static Specification<User> emailLike(String email) {
        return (Specification<User>) (root, criteria, cb) -> {
            if (email != null && !email.isEmpty()) {
                return cb.like(cb.lower(root.get("email")), email);
            } else {
                return null;
            }
        };
    }

}
