package cz.david.minirental.controllers;

import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.entity.User;
import cz.david.minirental.services.ProductService;
import cz.david.minirental.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AjaxController {

    private final UserService userService;
    private final ProductService productService;

    @Autowired
    public AjaxController(UserService userService, ProductService productService) {
        this.userService = userService;
        this.productService = productService;
    }

    /**
     * USER AUTOCOMPLETE
     **/
    @RequestMapping(value = "/ajax/customer/autocomplete", method = RequestMethod.POST)
    public ResponseEntity<?> customerAutocomplete(String value) {

        List<User> users = userService.findByLikeNameOrSurnameOrCompanyOrEmailOrPhone(value);
        return ResponseEntity.ok(users);
    }

    /**
     * PRODUCT AUTOCOMPLETE
     **/
    @RequestMapping(value = "/ajax/product/autocomplete", method = RequestMethod.POST)
    public ResponseEntity<?> productAutocomplete(String value) {

        List<Product> products = productService.findByLikeNameOrDescriptionOrId(value);
        return ResponseEntity.ok(products);
    }

}
