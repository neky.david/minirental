package cz.david.minirental.controllers;

import cz.david.minirental.model.entity.Mail;
import cz.david.minirental.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
public class HomeController extends BaseController {

    @Autowired
    private EmailService emailService;

    /**
     * CONTACT
     * */
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String contact(Model model) {
            /*try {
                Mail mail = new Mail();
                mail.setFrom("no-reply@minipujcovna.cz");
                mail.setTo("neky.d@seznam.cz");
                mail.setSubject("MiniPůjčovna - Aktivace účtu");

                Map<String, Object> mailModel = new HashMap<String, Object>();
                mailModel.put("guid", "GUIDGUIDGUIDGUIDGUID");
                mailModel.put("userId", "123456789");
                mail.setModel(mailModel);

                emailService.sendMail(mail, "mail/mailRegistration");

            } catch (Exception ex) {
                showError("EMAIL ERROR", model, ex);
            }*/

        return "contact";
    }

    /**
     * CONTACT MAIL
     */
    @RequestMapping(value = "/contact/mail", method = RequestMethod.POST)
    public String updateUser( @RequestParam String name,
                              @RequestParam String email,
                              @RequestParam String phoneNumber,
                              @RequestParam String mailMessage,
                              Model model){
        try {
            Mail mail = new Mail();
            mail.setFrom(email);
            mail.setTo("neky.d@seznam.cz");
            mail.setSubject("MiniPůjčovna - Zpráva od uživatele");

            Map<String, Object> mailModel = new HashMap<>();
            mailModel.put("name", name);
            mailModel.put("email", email);
            mailModel.put("phoneNumber", phoneNumber);
            mailModel.put("message", mailMessage);
            mail.setModel(mailModel);

            emailService.sendMail(mail, "mail/contactMail");
            showSuccess("Zpráva byla odeslána", model);
        }
        catch (Exception ex){
            showError("Nepodařilo se odeslat zprávu.", model, ex);
        }

        return "contact";
    }

    /**
     * ADMIN
     * */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin() {
        return "redirect:/admin/borrow/new";
    }
}
