package cz.david.minirental.controllers;

import cz.david.minirental.model.entity.Mail;
import cz.david.minirental.model.entity.User;
import cz.david.minirental.services.EmailService;
import cz.david.minirental.services.ProductService;
import cz.david.minirental.services.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
public class UserController extends BaseController {

    private final int PAGE_SIZE = 20;

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;

    public UserController(UserService userService, PasswordEncoder passwordEncoder, EmailService emailService) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.emailService = emailService;
    }

    /**
     * LOGIN
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        return "user/login";
    }

    /**
     * MENU LOGIN
     */
    @RequestMapping(value = "/menu/login", method = RequestMethod.POST)
    public String menuLogin(HttpServletRequest request,
                            @RequestParam(name = "email") String email,
                            @RequestParam(name = "password") String password,
                            @RequestParam(name = "origUrl") String url,
                            Model model,
                            RedirectAttributes redir) {

        try {
            request.login(email, password);
        } catch (Exception ex) {
            redir.addFlashAttribute("loginError", true);
        }

        return "redirect:" + url;
    }

    /**
     * LOGOUT
     **/
    @RequestMapping(value = "/menu/logout", method = RequestMethod.POST)
    public String menuLogout(@RequestParam(name = "origUrl") String url) {
        SecurityContextHolder.clearContext();

        return "redirect:" + url;
    }

    /**
     * LIST
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String newProduct(@RequestParam(required = false) Integer page,
                             @RequestParam(required = false) String name,
                             @RequestParam(required = false) String company,
                             @RequestParam(required = false) String email,
                             @RequestParam(required = false) String phone,
                             @RequestParam(required = false) String address,
                             Model model) {
        Pageable pageable;
        pageable = PageRequest.of(page != null ? page : 0, PAGE_SIZE, Sort.by("name"));

        Page<User> userPage = userService.getFilteredCustomers(true, name, company, email, phone, address, pageable);

        model.addAttribute("pageNumber", page != null ? page : 0);
        model.addAttribute("pageCount", userPage.getTotalPages());
        model.addAttribute("users", userPage.getContent());

        return "user/users";
    }

    /**
     * DETAIL USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/user/detail/{id}", method = RequestMethod.GET)
    public String detailUser(@PathVariable("id") Long id,
                             Model model) {
        model.addAttribute("user", userService.getById(id).orElse(null));

        return "user/userDetail";
    }

    /**
     * PROFILE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String currentUser(Model model) {

        model.addAttribute("user", User.getCurrentUser(userService));
        model.addAttribute("activeNav", "profile");

        return "user/userEdit";
    }

    /**
     * NEW
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/user/new", method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "user/userNew";
    }

    /**
     * ADD USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/user/add", method = RequestMethod.POST)
    public String addUser(HttpServletRequest request, @Valid @ModelAttribute("user") User user,
                          BindingResult bindingResult,
                          Model model,
                          RedirectAttributes redir) {

        if (bindingResult.hasErrors()) {
            return "user/userNew";
        }

        try {
            //kotroluje jestli už uživatel neexistuje
            Optional<User> exUser = userService.getUserByEmail(user.getEmail());
            if (exUser.isPresent()) {
                showError("Uživatel s emailem '" + user.getEmail() + "' již existuje", model);
                return "user/register";
            }

            user.setRegistered(false);
            userService.save(user);

            showSuccess("Zákazník byl úspšně vytvořen", redir);
            return "redirect:/admin/user/detail/" + user.getId();
        } catch (Exception ex) {
            showError("Nepodařilo se vytvořit uživatele", model, ex);
            return "user/userNew";
        }
    }

    /**
     * ADMIN PROFILE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/profile", method = RequestMethod.GET)
    public String adminProfile(Model model) {

        model.addAttribute("user", User.getCurrentUser(userService));
        model.addAttribute("activeNav", "admin");
        model.addAttribute("isProfile", true);

        return "user/userEdit";
    }

    /**
     * EDIT USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/user/edit/{id}", method = RequestMethod.GET)
    public String editUser(@PathVariable("id") Long id,
                           Model model) {
        model.addAttribute("user", userService.getById(id).orElse(null));
        model.addAttribute("activeNav", "admin");

        return "user/userEdit";
    }

    /**
     * UPDATE USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/user/update", method = RequestMethod.POST)
    public String updateUser(@ModelAttribute("user") @Valid User user,
                             BindingResult bindingResult,
                             Model model,
                             HttpServletRequest request,
                             RedirectAttributes redir) {

        if (bindingResult.hasErrors()) {
            return "user/userEdit";
        }

        //kontrola práv
        if (request.isUserInRole("ADMIN") || User.getCurrentUser().getId().equals(user.getId())) {
            //kotroluje zda neexistuje uživatel se stejným mailem
            Optional<User> exUser = userService.getUserByEmail(user.getEmail());
            if (exUser.isPresent() && !exUser.get().getId().equals(user.getId())) {
                model.addAttribute("user", user);
                model.addAttribute("emailErrorMessage", "Uživatel se stejnou emailovou adresou již existuje");
                return "user/userEdit";
            } else {
                exUser = userService.getById(user.getId());

                if (exUser.isPresent()) {
                    User origUser = exUser.get();

                    try {
                        //new password
                        if (user.getPassword().isEmpty()) {
                            user.setPassword(origUser.getPassword());
                        } else {
                            user.setPassword(passwordEncoder.encode(user.getPassword()));
                        }

                        userService.update(user);

                        showSuccess("Uživatel byl úspěšně aktualizován", redir);

                    } catch (Exception ex) {
                        showError("Nepodařilo se upravit uživatele " + origUser.getEmail(), model, ex);
                        model.addAttribute("user", user);
                        return "user/userEdit";
                    }

                    if(User.getCurrentUser().getId().equals(user.getId())){
                        return "redirect:/admin/profile";
                    }
                    return "redirect:/admin/user/edit/" + user.getId();

                } else {
                    model.addAttribute("user", user);
                    showError("Nepodařilo se najít uživatele s id " + user.getId(), model);
                    return "user/userEdit";
                }
            }
        } else {
            return "errors/403";
        }
    }

    /**
     * REMOVE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/user/remove/{id}", method = RequestMethod.GET)
    public String removeUser(@PathVariable("id") Long id,
                                RedirectAttributes redir) {

        User user = userService.getById(id).orElse(null);
        if(user != null) {
            userService.remove(user);

            showSuccess("Zákazník " + user.getFullName() + " byl úspěšně smázán.", redir);
        }
        else {
            showError("Nepodařilo se smazat zákazníka", redir);
        }

        return "redirect:/admin/users";
    }

    /**
     * ACIVATE USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/user/activate", method = RequestMethod.GET)
    public String activateUser(@RequestParam(required = false) Long id,
                               @RequestParam(required = false) String guid,
                               Model model,
                               RedirectAttributes redir) {

        //pokud se uživ. přihlásí ze stránky /user/active tak ho to přesměruje na index
        if (id == null && guid == null) {
            if (model.containsAttribute("loginError"))
                redir.addFlashAttribute("loginError", true);
            return "redirect:/";
        }

        boolean success = false;

        Optional<User> userOptional = userService.getById(id);

        if (userOptional.isPresent()) {
            User user = userOptional.get();

            if (user.getGuid().equals(guid)) {
                user.setActive(true);
                userService.update(user);
                success = true;
            }
        } else {
            showError("uživateln nebyl nalezen.", model);
        }

        model.addAttribute("success", success);
        return "user/userActive";
    }
}
