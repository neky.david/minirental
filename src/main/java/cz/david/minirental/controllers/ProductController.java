package cz.david.minirental.controllers;

import cz.david.minirental.model.entity.Accessory;
import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.Image;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.enums.ProductCategory;
import cz.david.minirental.helpers.CalendarHelper;
import cz.david.minirental.helpers.ImageHelper;
import cz.david.minirental.services.BorrowService;
import cz.david.minirental.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.*;

import static cz.david.minirental.MinirentalApplication.AJAX_HEADER_NAME;
import static cz.david.minirental.MinirentalApplication.AJAX_HEADER_VALUE;

@Controller
public class ProductController extends BaseController {

    private final int PAGE_SIZE = 20;

    private final ProductService productService;
    private final BorrowService borrowService;

    @Autowired
    public ProductController(ProductService productService, BorrowService borrowService) {
        this.productService = productService;
        this.borrowService = borrowService;
    }

    /**
     * SORTIMENT
     * */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(@PathParam(value = "cat") String cat,
                        Model model) {
        fillSortimentModel(cat, model);
        return "product/sortiment";
    }

    /**
     * AJAX - SORTIMENT
     * */
    @RequestMapping(value = "/ajax/sortiment", method = RequestMethod.POST)
    public String ajaxSortiment(String cat, Model model) {

        fillSortimentModel(cat, model);
        return "product/sortiment :: products";
    }

    private void fillSortimentModel(String cat, Model model){
        ProductCategory category = ProductCategory.MACHINES;
        if (cat != null && !cat.isEmpty())
            category = ProductCategory.valueOf(cat.toUpperCase());

        List<Product> products = productService.getActiveByCategory(category);
        model.addAttribute("products", products);
        //model.addAttribute("products", new ArrayList<>());
        //model.addAttribute("availableDates", Product.getStringsAvailableDates(products));
        model.addAttribute("availableDates", borrowService.getStringsAvailableDates(products));
        model.addAttribute("todayDate", CalendarHelper.getFormatString(CalendarHelper.removeTime(Calendar.getInstance()), false));
        model.addAttribute("heading", category.getHeading());
        model.addAttribute("category", category.toString().toLowerCase());
    }

    /**
     * DETAIL
     */
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public String detailUser(@PathVariable("id") Long id,
                           Model model) {

        Product product = productService.getById(id).orElse(null);
        if(product != null) {
            model.addAttribute("product", product);
            model.addAttribute("borrow", new Borrow());
            model.addAttribute("unavailableDates", borrowService.getUnavailableDates(product));
            model.addAttribute("availableDate", borrowService.getFirstAvailableDate(product));
        }
        else {
            showError("Nepodařilo se najít produkt s id " + id, model);
        }

        return "product/productDetail";
    }


    /*------------------------------------ADMIN------------------------------------*/

    /**
     * LIST
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/products", method = RequestMethod.GET)
    public String products(@RequestParam(required = false) Integer page,
                          @RequestParam(required = false) String name,
                           @RequestParam(required = false) String category,
                          Model model) {

        Pageable pageable = PageRequest.of(page != null ? page : 0, PAGE_SIZE, Sort.by("name"));

        Page<Product> productPage = productService.getFiltredProducts(true, name, category, pageable);

        model.addAttribute("pageNumber", page != null ? page : 0);
        model.addAttribute("pageCount", productPage.getTotalPages());
        model.addAttribute("products", productPage.getContent());

        return "product/products";
    }

    /**
     * NEW
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/new", method = RequestMethod.GET)
    public String newProduct(@RequestParam(value = "cat", required = false) String category, Model model) {

        Product product = new Product();
        product.setAccessories(new ArrayList<>());
        product.getAccessories().add(new Accessory());
        if (category != null && !category.equals("null"))
            product.setCategory(ProductCategory.valueOf(category.toUpperCase()));
        model.addAttribute("product", product);
        return "product/productNew";
    }

    /**
     * ADD
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/add", method = RequestMethod.POST)
    @ResponseBody
    public String addProduct(HttpServletRequest request,
                             @Valid @ModelAttribute("product") Product product,
                             BindingResult bindingResult,
                             @RequestParam("imgIndex") int imgIndex,
                             @RequestParam(value = "btn") String btn, //next/save    /*@RequestParam("files") MultipartFile[] files,*/
                             Model model) { //@ModelAttribute("user") Product product, @RequestParam("images") MultipartFile[] images,


        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {

            // It is an Ajax request, render only #accessories fragment of the page.
            String type = "redirect";
            String data = "/";

            try {

                if (bindingResult.hasErrors()) {
                    type = "error";
                    FieldError error = (FieldError) bindingResult.getAllErrors().get(0);
                    data = error.getField() + " - " + error.getDefaultMessage();
                } else {
                    try {
                        if (request instanceof MultipartHttpServletRequest)
                            product.setImages((MultipartHttpServletRequest) request);
                    } catch (IOException ex) {
                        logger.error("MINIRENTAL_LOG: Nepodařilo se uložit obrázek", ex);
                        type = "error";
                        data = "Nepodařilo se uložit obrázek";
                    }

                    setMainImage(product, imgIndex);

                    productService.save(product);

                    if (btn.equals("next")) {
                        data = "/admin/product/new";
                    } else if (btn.equals("save")) {
                        data = "/product/" + product.getId();
                    }
                }
            } catch (Exception ex) {
                logger.error("MINIRENTAL_LOG: Během uložení produktu nastala neočekávaná chyba", ex);
                type = "error";
                data = "Během uložení nastala neočekávaná chyba.";
            }

            //type - redirect/error
            return "{\"type\":\"" + type + "\", \"data\":\"" + data + "\"}";
        } else {
            // It is a standard HTTP request, render whole page.
            return "<script>window.location.href = \"/\";</script>";
        }
    }

    private void setMainImage(Product product, int imgIndex){
        if (product.getImages() != null && product.getImages().size() > 0) {
            if (imgIndex >= 0)
                product.setMainImageName(product.getImages().get(imgIndex).getName());
            else
                product.setMainImageName(product.getImages().get(0).getName());
        }
    }

    /**
     * EDIT
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/edit/{id}", method = RequestMethod.GET)
    public String editProduct(@PathVariable("id") Long id,
                              Model model) {
        model.addAttribute("product", productService.getById(id).orElse(null));

        return "product/productEdit";
    }

    /**
     * UPDATE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateProduct(HttpServletRequest request,
                                @Valid @ModelAttribute("product") Product product,
                                BindingResult bindingResult,
                                @RequestParam("imgIndex") int imgIndex,
                                @RequestParam("savedImages") String saveImages,
                                @RequestParam(value = "btn") String btn, //next/save    /*@RequestParam("files") MultipartFile[] files,*/
                                Model model) {
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {

            // It is an Ajax request, render only #accessories fragment of the page.
            String type = "redirect";
            String data = "/";

            try {

                if (bindingResult.hasErrors()) {
                    type = "error";
                    FieldError error = (FieldError) bindingResult.getAllErrors().get(0);
                    data = error.getField() + " - " + error.getDefaultMessage();
                } else {

                    Product origProduct = productService.getById(product.getId()).orElse(null);
                    if (origProduct != null) {

                        //smazání odstraněných obrázků
                        for (Image delImg : origProduct.getImages()) {
                            if (!saveImages.contains(delImg.getName()))
                                delImg.removeImageFiles();
                        }

                        //zachování původních obrázků
                        List<Image> origImages = new ArrayList<>();
                        if (saveImages != null && !saveImages.isEmpty()) {
                            String[] origImgsName = saveImages.split(";");
                            if (origImgsName.length > 0) {
                                for (String sImg : origImgsName) {
                                    origImages.add(new Image(sImg, request.getServletContext().getRealPath("/uploads/" + sImg)));
                                }
                            }
                        }

                        //uložení nových obrázků
                        List<Image> newImages = new ArrayList<>();
                        try {
                            if (request instanceof MultipartHttpServletRequest)
                                newImages = ImageHelper.saveMultipartImage((MultipartHttpServletRequest) request);
                        } catch (IOException ex) {
                            logger.error("MINIRENTAL_LOG: Nepodařilo se uložit obrázek", ex);
                            type = "error";
                            data = "Nepodařilo se uložit obrázek";
                        }

                        List<Image> images = new ArrayList<>();
                        images.addAll(origImages);
                        images.addAll(newImages);
                        product.setImages(images);

                        //main img
                        setMainImage(product, imgIndex);

                        productService.update(product);

                        if (btn.equals("next")) {
                            data = "/admin/product/new";
                        } else if (btn.equals("save")) {
                            data = "/product/" + product.getId();
                        }
                    } else {
                        type = "error";
                        data = "Nepodařilo se nalézt produkt s Id " + product.getId();
                    }
                }
            } catch (Exception ex) {
                logger.error("MINIRENTAL_LOG: Během uložení produktu nastala neočekávaná chyba", ex);
                type = "error";
                data = "Během uložení nastala neočekávaná chyba.";
            }

            //type - redirect/error
            return "{\"type\":\"" + type + "\", \"data\":\"" + data + "\"}";
        } else {
            // It is a standard HTTP request, render whole page.
            return "<script>window.location.href = \"/\";</script>";
        }
    }

    /**
     * ADMIN DETAIL - borrow/return
     */
    /*@PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/{id}", method = RequestMethod.GET)
    public String product(@PathVariable("id") Long id,
                             Model model) {
        Product product = productService.getById(id).orElse(null);
        if(product != null) {
            model.addAttribute("product", product);
            model.addAttribute("borrow", new Borrow());
            model.addAttribute("unavailableDates", borrowService.getUnavailableDates(product));
            model.addAttribute("availableDate", borrowService.getFirstAvailableDate(product));
        }
        else {
            showError("Nepodařilo se najít produkt s id " + id, model);
        }

        return "product/adminProduct";
    }*/

    /**
     * REMOVE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/remove/{id}", method = RequestMethod.GET)
    public String removeProduct(@PathVariable("id") Long id,
                                RedirectAttributes redir) {

        Product product = productService.getById(id).orElse(null);
        productService.remove(id);

        redir.addFlashAttribute("successMessage", "Produkt " + product.getName() + " byl úspěšně smázán.");

        return "redirect:/admin/products";
    }

    /**
     * ADD ACCESSORY
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/accessory/add", method = RequestMethod.POST)
    public String addAccessory(Product product, HttpServletRequest request) {
        if (product.getAccessories() == null)
            product.setAccessories(new ArrayList<>());
        product.getAccessories().add(new Accessory());
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            // It is an Ajax request, render only #accessories fragment of the page.
            return "productNew::#accessories";
        } else {
            // It is a standard HTTP request, render whole page.
            return "productNew";
        }
    }

    /**
     * REMOVE ACCESSORY
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/accessory/remove", method = RequestMethod.POST)
    public String removeOrder(Product product, @RequestParam("index") int index, HttpServletRequest request) {
        if (product.getAccessories() != null)
            product.getAccessories().remove(index);
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            return "productNew::#accessories";
        } else {
            return "productNew";
        }
    }
}
