package cz.david.minirental.controllers;

import cz.david.minirental.exceptions.ForbiddenException;
import cz.david.minirental.helpers.CalendarHelper;
import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.entity.User;
import cz.david.minirental.services.BorrowService;
import cz.david.minirental.services.ProductService;
import cz.david.minirental.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Optional;

@Controller
public class BorrowController extends BaseController {

    private final int PAGE_SIZE = 20;

    private final BorrowService borrowService;
    private final UserService userService;
    private final ProductService productService;

    @Autowired
    public BorrowController(BorrowService borrowService, UserService userService, ProductService productService) {
        this.borrowService = borrowService;
        this.userService = userService;
        this.productService = productService;
    }

    /**
     * LIST
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/borrows", method = RequestMethod.GET)
    public String borrows(Model model) {

        User currentUser = User.getCurrentUser(userService);
        if (currentUser == null) {
            showError("Nepodařilo se vyhledat přihlášeného uživatele", model);
            return "borrow/userBorrows";
        }

        model.addAttribute("borrows", borrowService.getComingByUser(currentUser));
        model.addAttribute("oldBorrows", borrowService.getPastByUser(currentUser));

        return "borrow/userBorrows";
    }

    /**
     * DETAIL
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/borrow/detail/{id}", method = RequestMethod.GET)
    public String detailBorrow(@PathVariable("id") Long id,
                               Model model) {
        Borrow borrow = borrowService.getById(id).orElse(null);
        model.addAttribute("activeNav", "user");

        if (borrow == null) {
            showError("Nepodařilo se najít rezervaci s id " + id, model);
            return "borrow/userBorrowDetail";
        }

        if (borrow.getUser().getId().equals(User.getCurrentUser(userService).getId())) {
            model.addAttribute("borrow", borrow);
            return "borrow/userBorrowDetail";
        }

        throw new ForbiddenException();
    }

    /*------------------------------------ADMIN------------------------------------*/

    /**
     * ADMIN LIST RESERVATIONS
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/reservations", method = RequestMethod.GET)
    public String reservations(@RequestParam(required = false) Integer page,
                               @RequestParam(required = false) String createDate,
                               @RequestParam(required = false) String fromToDate,
                               @RequestParam(required = false) String userName,
                               @RequestParam(required = false) String productName,
                               Model model) {

        fillAdminListModel(model, page, createDate, fromToDate, userName, productName, true);

        return "borrow/reservations";
    }

    /**
     * ADMIN LIST BORROWS
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrows", method = RequestMethod.GET)
    public String borrows(@RequestParam(required = false) Integer page,
                          @RequestParam(required = false) String createDate,
                          @RequestParam(required = false) String fromToDate,
                          @RequestParam(required = false) String userName,
                          @RequestParam(required = false) String productName,
                          Model model) {

        fillAdminListModel(model, page, createDate, fromToDate, userName, productName, false);

        return "borrow/borrows";
    }

    private void fillAdminListModel(Model model, Integer page, String createDate, String fromToDate, String userName, String productName, boolean isReservation) {
        Pageable pageable = PageRequest.of(page != null ? page : 0, PAGE_SIZE, Sort.by("createDate").descending());

        Page<Borrow> borrowPage = borrowService.getFiltredBorrows(
                true, isReservation,
                CalendarHelper.getCalendarFromRangeString(createDate, 0),
                CalendarHelper.getCalendarFromRangeString(createDate, 1),
                CalendarHelper.getCalendarFromRangeString(fromToDate, 0),
                CalendarHelper.getCalendarFromRangeString(fromToDate, 1),
                userName, productName, pageable);

        model.addAttribute("createDate", createDate);
        model.addAttribute("fromToDate", fromToDate);

        model.addAttribute("isReservations", isReservation);
        model.addAttribute("pageNumber", page != null ? page : 0);
        model.addAttribute("pageCount", borrowPage.getTotalPages());
        model.addAttribute("borrows", borrowPage.getContent());
    }

    /**
     * ADMIN DETAIL
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/detail/{id}", method = RequestMethod.GET)
    public String adminDetailBorrow(HttpServletRequest request,
                                    @PathVariable("id") Long id,
                                    Model model) {
        Borrow borrow = borrowService.getById(id).orElse(null);

        if (borrow == null) {
            showError("Nepodařilo se najít rezervaci s id " + id, model);
            return "borrow/borrowDetail";
        }

        model.addAttribute("borrow", borrow);
        return "borrow/borrowDetail";
    }

    /**
     * NEW
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/new", method = RequestMethod.GET)
    public String product(@RequestParam(name = "pid", required = false) Long productId,
                          @RequestParam(name = "uid", required = false) Long userId,
                          Model model) {

        Borrow borrow = new Borrow();

        //PRODUCT
        Product product = null;
        if (productId != null) {
            Optional<Product> OptP = productService.getById(productId);
            if (OptP.isPresent()) {
                product = OptP.get();
                borrow.setProduct(product);
                model.addAttribute("unavailableDates", borrowService.getUnavailableDates(product));
                model.addAttribute("availableDate", borrowService.getFirstAvailableDate(product));
            }
        }
        model.addAttribute("product", product);

        //USER
        User user = null;
        if (userId != null) {
            Optional<User> OptU = userService.getById(userId);
            if (OptU.isPresent()) {
                user = OptU.get();
                borrow.setUser(user);
            }
        }
        model.addAttribute("user", user);

        model.addAttribute("borrow", borrow);
        return "borrow/borrowNew";
    }

    /**
     * ADD
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/add", method = RequestMethod.POST)
    public String addBorrow(HttpServletRequest request,
                            @Valid @ModelAttribute("borrow") Borrow borrow,
                            @RequestParam("isReservation") boolean isReservation,
                            BindingResult bindingResult,
                            Model model,
                            RedirectAttributes redir) {

        if (bindingResult.hasErrors()) {
            return fillAddErrorModelAndReturnView(model, borrow);
        }

        if (borrow.getUser() == null || borrow.getUser().getId() < 1) {
            showError("Nepodařilo se vytvořit výpůjčku - nebyl nalezen uživatel", model);
            return fillAddErrorModelAndReturnView(model, borrow);
        }

        //zkontroluje zda jsou datumy volné
        String msg = borrow.checkDatesValidity(borrowService);
        if (!msg.isEmpty()) {
            model.addAttribute("borrowErrorMessage", msg);
            return fillAddErrorModelAndReturnView(model, borrow);
        }

        borrow.setReservation(isReservation);
        borrowService.save(borrow);

        if (isReservation)
            showSuccess("Rezervace byla úspěšně vytvořena", redir);
        else
            showSuccess("Výpůjčka byla úspěšně vytvořena", redir);

        return "redirect:/admin/borrow/detail/" + borrow.getId();
    }

    private String fillAddErrorModelAndReturnView(Model model, Borrow borrow) {
        Product product = productService.getById(borrow.getProduct().getId()).orElse(null);
        model.addAttribute("product", product);
        model.addAttribute("borrow", borrow);
        model.addAttribute("unavailableDates", borrowService.getUnavailableDates(product));
        model.addAttribute("availableDate", borrowService.getFirstAvailableDate(product));
        return "borrow/borrowNew";
    }

    /**
     * ADMIN ADD FROM RESERVATION
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/reservation/borrow", method = RequestMethod.GET)
    public String addBorrowFromReservation(@RequestParam("reservId") Long id,
                                           Model model,
                                           RedirectAttributes redir) {
        if (borrowService.setReservation(id, false)) {
            showSuccess("Z rezervace byla úspěšně vytvořena výpůjčka", redir);
            return "redirect:/admin/borrow/detail/" + id;
        } else {
            showError("Nepodařilo se vytvořit výpůjčku z rezervace s id " + id, redir);
            return "redirect:/admin/borrow/detail/" + id;
        }
    }

    /**
     * RETURN
     * */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/return/{id}", method = RequestMethod.GET)
    public String addBorrowFromReservation(@PathVariable("id") Long id,
                                           @RequestParam(required = false) boolean list,
                                           Model model,
                                           RedirectAttributes redir) {
        if (borrowService.returnBorrow(id)) {
            showSuccess("Produkt byl úspěšně vrácen", redir);
            if(list){
                return "redirect:/admin/borrows";
            }
            return "redirect:/admin/borrow/detail/" + id;
        } else {
            showError("Nepodařilo se vrátit produkt", redir);
            if(list)
                return "redirect:/admin/borrows";
            return "redirect:/borrow/detail/" + id;
        }
    }

    /**
     * EDIT
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/edit/{id}", method = RequestMethod.GET)
    public String editBorrow(@PathVariable("id") Long id,
                             Model model) {
        Optional<Borrow> optB = borrowService.getById(id);
        if (optB.isPresent()) {
            Borrow borrow = optB.get();
            model.addAttribute("borrow", borrow);
            model.addAttribute("unavailableDates", borrowService.getUnavailableDatesExceptBorrow(borrow.getProduct(), borrow));
            model.addAttribute("availableDate", borrowService.getFirstAvailableDate(borrow.getProduct()));
        } else {
            showError("Nepodařilo se najít rezervaci/výpůjčku", model);
        }

        return "borrow/borrowEdit";
    }


    /**
     * UPDATE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/update", method = RequestMethod.POST)
    public String updateBorrow(HttpServletRequest request,
                               @Valid @ModelAttribute("borrow") Borrow borrow,
                               Model model,
                               RedirectAttributes redir) {

        //zkontroluje zda existuje zákazník
        if (borrow.getProduct() == null || !productService.getById(borrow.getProduct().getId()).isPresent()) {
            showError("Nepodařilo se upravit výpůjčku/rezervaci - nebyl nalezen produkt", redir);
            return "redirect:/admin/borrow/edit/" + borrow.getId();
        }

        //zkontroluje zda existuje zákazník
        if (borrow.getUser() == null || !borrowService.getById(borrow.getUser().getId()).isPresent()) {
            showError("Nepodařilo se upravit výpůjčku/rezervaci - nebyl nalezen zákazník", redir);
            return "redirect:/admin/borrow/edit/" + borrow.getId();
        }

        //zkontroluje zda jsou datumy volné
        String msg = borrow.checkDatesValidityExceptBorrow(borrowService, borrow);
        if (!msg.isEmpty()) {
            model.addAttribute("borrowErrorMessage", msg);
            Product product = productService.getById(borrow.getProduct().getId()).orElse(null);
            borrow.setProduct(product);
            model.addAttribute("borrow", borrow);
            model.addAttribute("unavailableDates", borrowService.getUnavailableDatesExceptBorrow(product, borrow));
            model.addAttribute("availableDate", borrowService.getFirstAvailableDate(product));
            return "borrow/borrowEdit";
        }

        try {
            borrowService.update(borrow);
        } catch (Exception ex) {
            showError("Nepodařilo se upravit výpůjčku/rezervaci", redir, ex);
            return "redirect:/admin/borrow/edit/" + borrow.getId();
        }

        if (borrow.isReservation())
            showSuccess("Rezervace byla úspěšně upravena", redir);
        else
            showSuccess("Výpůjčka byla úspěšně upravena", redir);

        return "redirect:/admin/borrow/detail/" + borrow.getId();
    }

    /**
     * REMOVE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/remove/{id}", method = RequestMethod.GET)
    public String removeProduct(@PathVariable("id") Long id,
                                RedirectAttributes redir) {

        try {
            Optional<Borrow> optB = borrowService.getById(id);

            if (optB.isPresent()) {
                Borrow borrow = optB.get();

                borrowService.remove(borrow);

                if (borrow.isReservation()) {
                    showSuccess("Rezervace produktu " + borrow.getProduct().getName() + " byla úspěšně smázána.", redir);
                    return "redirect:/admin/reservations";
                } else {
                    showSuccess("Výpůjčka produktu " + borrow.getProduct().getName() + " byla úspěšně smázána.", redir);
                    return "redirect:/admin/borrows";
                }
            }
        } catch (Exception ex) {
            showError("Nepodařilo se smazat rezervaci/výpůjčku", redir, ex);
        }

        return "redirect:/admin/reservations";
    }

    /**
     * NEW - REFRESH PRODUCT PART
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/new/productpart", method = RequestMethod.POST)
    public String getBorrowProductPart(Long productId, Model model) {
        Product product = null;
        Optional<Product> OptP = productService.getActiveById(productId);
        if (OptP.isPresent()) {
            product = OptP.get();
            model.addAttribute("unavailableDates", borrowService.getUnavailableDates(product));
            model.addAttribute("availableDate", borrowService.getFirstAvailableDate(product));
        }

        model.addAttribute("product", product);
        model.addAttribute("borrow", new Borrow());

        return "borrow/borrowNew :: productPart(borrow = ${borrow})";
    }

}
