package cz.david.minirental.interceptors;

import cz.david.minirental.model.entity.User;
import cz.david.minirental.services.UserService;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class Interceptor implements HandlerInterceptor {

    private final UserService userService;

    public Interceptor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) {

        if(modelAndView != null) {
            modelAndView.getModel().put("currentUser", User.getCurrentUser(userService));
        }

    }
}
