package cz.david.minirental.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalendarHelper {

    public static Calendar removeTime(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    public static Date addDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 1);
        return cal.getTime();
    }

    public static Calendar getCalendarFromRangeString(String value, int index) {
        if(value != null && !value.isEmpty()){
            String[] strings = value.split(" - ");
            if(strings.length > index){
                return getCalendarFromString(strings[index]);
            }
        }
        return null;
    }

    public static Calendar getCalendarFromString(String value) {
        return getCalendarFromString(value, "dd.MM.yyyy");
    }

    public static Calendar getCalendarFromString(String value, String pattern) {
        Calendar cal = null;
        if(value != null && !value.isEmpty()) {
            try {
                cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.GERMANY);
                cal.setTime(sdf.parse(value));
            }
            catch (Exception ignored){
                cal = null;
            }
        }
        return cal;
    }

    public static String getFormatString(Calendar cal) {
        return getFormatString(cal, "dd.MM.yyyy HH:mm:ss");
    }

    public static String getFormatString(Calendar cal, boolean time) {
        if (time)
            return getFormatString(cal, "dd.MM.yyyy HH:mm:ss");
        else
            return getFormatString(cal, "dd.MM.yyyy");
    }

    public static String getFormatString(Calendar cal, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(cal.getTime());
    }

    public static Calendar getYesterday() {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        return yesterday;
    }
}
