package cz.david.minirental.helpers;

import cz.david.minirental.model.entity.Image;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ImageHelper {

    private static final int SMALL_IMG_WIDTH = 400;
    private static final int SMALL_IMG_HEIGHT = 400;
    private static final int THUMB_IMG_WIDTH = 120;
    private static final int THUMB_IMG_HEIGHT = 120;

    public static List<Image> saveMultipartImage(MultipartHttpServletRequest request) throws IOException {
        List<Image> images = new ArrayList<>();

        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();

        // Iterate through the map
        for (MultipartFile multipartFile : fileMap.values()) {

            String name = UUID.randomUUID().toString() + getExtensionFromMime(multipartFile.getContentType());

            String filePath = request.getServletContext().getRealPath("/uploads/" + name);
            String dirPath;

            if(filePath.contains("\\")){
                dirPath = filePath.substring(0, filePath.lastIndexOf("\\"));
            }
            else {
                dirPath = filePath.substring(0, filePath.lastIndexOf("/"));
            }

            File dir = new File(dirPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            multipartFile.transferTo(new File(filePath));

            Image img = saveConvertedImg(filePath);
            img.setName(name);
            String smallPath = filePath.substring(0, filePath.lastIndexOf(".")) + ".small.jpg";
            saveResizeImg(filePath, smallPath, SMALL_IMG_WIDTH, SMALL_IMG_HEIGHT);
            String thumbPath = filePath.substring(0, filePath.lastIndexOf(".")) + ".thumb.jpg";
            saveResizeImg(filePath, thumbPath, THUMB_IMG_WIDTH, THUMB_IMG_HEIGHT);
            //img.setSmallPath(saveCompressedImg(filePath));

            //odstraní původní obrázek
            if (!filePath.equals(img.getPath()))
                deleteImage(filePath);

            images.add(img);
        }

        return images;
    }

    public static boolean deleteImage(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    /**
     * Uloží obrázek ve formátu .jpg
     * redukuje velikost obrázku
     * vrací nový filePath k obrázku
     */
    private static Image saveConvertedImg(String filePath) {

        Image img = new Image();

        String outputFileName = filePath.substring(0, filePath.lastIndexOf(".")) + ".jpg";

        img.setPath(outputFileName);

        try {
            File input = new File(filePath);
            File output = new File(outputFileName);

            BufferedImage image = ImageIO.read(input);

            img.setWidth(image.getWidth());
            img.setHeight(image.getHeight());

            BufferedImage result = new BufferedImage(
                    image.getWidth(),
                    image.getHeight(),
                    BufferedImage.TYPE_INT_RGB);
            result.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
            ImageIO.write(result, "jpg", output);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return img;
    }

    private static Image saveResizeImg(String filePath, String newFilePath, int width, int height) throws IOException {
        Image img = new Image();

        BufferedImage in = javax.imageio.ImageIO.read(new java.io.File(filePath));
        BufferedImage out = scaleImage(in, width, height);

        img.setHeight(out.getHeight());
        img.setWidth(out.getWidth());
        img.setPath(newFilePath);

        javax.imageio.ImageIO.write(out, "JPG", new java.io.File(newFilePath));

        return img;
    }

    private static BufferedImage scaleImage(BufferedImage image, int newWidth, int newHeight) {
        // Make sure the aspect ratio is maintained, so the image is not distorted
        double thumbRatio = (double) newWidth / (double) newHeight;
        int imageWidth = image.getWidth(null);
        int imageHeight = image.getHeight(null);
        double aspectRatio = (double) imageWidth / (double) imageHeight;

        if (thumbRatio < aspectRatio) {
            newHeight = (int) (newWidth / aspectRatio);
        } else {
            newWidth = (int) (newHeight * aspectRatio);
        }

        // Draw the scaled image
        BufferedImage newImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = newImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.drawImage(image, 0, 0, newWidth, newHeight, null);

        return newImage;
    }

    /**
     * Uloží obrázek komprimovaný obrázek
     * vrací nový filePath k obrázku
     */
    private static String saveCompressedImg(String filePath) throws IOException {

        File input = new File(filePath);
        BufferedImage image = ImageIO.read(input);

        String outputFilePath = filePath.substring(0, filePath.lastIndexOf(".")) + ".small.jpg";
        File output = new File(outputFilePath);
        OutputStream out = new FileOutputStream(output);

        ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
        ImageOutputStream ios = ImageIO.createImageOutputStream(out);
        writer.setOutput(ios);

        ImageWriteParam param = writer.getDefaultWriteParam();
        if (param.canWriteCompressed()) {
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(0.1f);
        }

        writer.write(null, new IIOImage(image, null, null), param);

        out.close();
        ios.close();
        writer.dispose();

        return outputFilePath;
    }

    private static String getExtensionFromMime(String mime) {
        if (mime != null) {
            switch (mime) {
                case "image/bmp":
                    return ".bmp";
                case "image/gif":
                    return ".gif";
                case "image/jpeg":
                    return ".jpg";
                case "image/png":
                    return ".png";
                case "image/svg+xml":
                    return ".svg";
                case "image/tiff":
                    return ".tiff";
                case "image/vnd.microsoft.icon":
                    return ".ico";
                case "image/webp":
                    return ".webp";
            }
        }
        return "";
    }
}
