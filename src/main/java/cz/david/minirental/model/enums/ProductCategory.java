package cz.david.minirental.model.enums;

public enum ProductCategory {
    MACHINES,
    TOOLS,
    TRANSPORT,
    SPORT;

    public String getHeading(){
        switch (this){
            case MACHINES:{
                return "Stavební stroje";
            }
            case TOOLS:{
                return "Stavební nářadí";
            }
            case TRANSPORT:{
                return "Dopravní prostředky";
            }
            case SPORT:{
                return "Sport a zábava";
            }
        }
        return "";
    }
}
