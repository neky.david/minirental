package cz.david.minirental.model.entity;

import cz.david.minirental.helpers.CalendarHelper;
import cz.david.minirental.services.BorrowService;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.joda.time.Interval;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Entity
@Table(name = "borrow")
public class Borrow extends BaseEntity {

    private boolean reservation;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @NotNull
    private GregorianCalendar fromDate;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @NotNull
    private GregorianCalendar toDate;

    @DateTimeFormat(pattern = "dd.MM.yyyy HH.mm.ss")
    //datum a čas vrácení produktu
    private Calendar returnDate;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @LazyCollection(LazyCollectionOption.FALSE)
    private User user;

    @ManyToOne(targetEntity = Product.class, fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @LazyCollection(LazyCollectionOption.FALSE)
    private Product product;

    @ManyToMany(targetEntity = Accessory.class, cascade = CascadeType.DETACH)
    @Valid
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "borrow_accessories")
    private List<Accessory> accessories;

    /**
     * Zjistí zda se datum nepřekrývá s jinou rezervací
     */
    public String checkDatesValidityExceptBorrow(BorrowService borrowService, Borrow borrow) {
        String msg = "";
        Calendar today = CalendarHelper.removeTime(Calendar.getInstance());

        if (fromDate == null || toDate == null)
            return "Chybí datum vypůjčení";
        if (fromDate.before(today))
            return "Počáteční datum vypůjčení je neplatné";
        if (toDate.before(today))
            return "Konečné datum vypůjčení je neplatné";

        List<Borrow> borrows = borrowService.getComingByProduct(product);
        Calendar date1 = Calendar.getInstance();
        date1.setTimeInMillis(fromDate.getTimeInMillis());
        //date1.add(Calendar.DATE, -1);

        Calendar date2 = Calendar.getInstance();
        date2.setTimeInMillis(toDate.getTimeInMillis());
        //date2.add(Calendar.DATE, 1);

        if (borrows != null && borrows.size() > 0) {
            for (Borrow b : borrows) {
                //přeskočí zadaný borrow.. kvůli editaci
                if (borrow == null || !b.getId().equals(borrow.getId())) {

                    Interval interval = new Interval(date1.getTimeInMillis(), date2.getTimeInMillis());
                    Interval interval2 = new Interval(b.fromDate.getTimeInMillis(), b.toDate.getTimeInMillis());

                    if (interval.overlaps(interval2)) {
                        return "Nelze vytvořit výpůjčku: v daném rozmezí je již produkt vypůjčen.";
                    }
                }
            }
        }
        return msg;
    }

    //smaže accessories bez ID
    public void removeEmptyAccessories() {
        if (accessories != null) {
            for (int i = 0; i < accessories.size(); i++) {
                Accessory accessory = accessories.get(i);
                if (accessory.getId() == null || accessory.getId() < 1) {
                    accessories.remove(accessory);
                    i--;
                }
            }
        }
    }

    public boolean isAccessorySelected(Accessory accessory) {
        if (accessory != null && accessories != null) {
            for (Accessory a :
                    accessories) {
                if (a.getId() != null && accessory.getId() != null &&
                            a.getId().equals(accessory.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public String checkDatesValidity(BorrowService borrowService) {
        return checkDatesValidityExceptBorrow(borrowService, null);
    }

    public static Calendar getFirstAvailableDate(List<Borrow> borrows) {
        Calendar date = Calendar.getInstance();
        for (Borrow b : borrows) {
            if (date.before(b.getFromDate())) {
                break;
            } else {
                date = b.getToDate();
                date.add(Calendar.DATE, 1);
            }
        }
        return date;
    }

    public String getFormatFromDate() {
        return CalendarHelper.getFormatString(fromDate, "dd.MM.yyyy");
    }

    public String getFormatToDate() {
        return CalendarHelper.getFormatString(toDate, "dd.MM.yyyy");
    }

    public GregorianCalendar getFromDate() {
        return fromDate;
    }

    public void setFromDate(GregorianCalendar fromDate) {
        this.fromDate = fromDate;
    }

    public GregorianCalendar getToDate() {
        return toDate;
    }

    public void setToDate(GregorianCalendar toDate) {
        this.toDate = toDate;
    }

    public Calendar getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Calendar returnDate) {
        this.returnDate = returnDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Accessory> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Accessory> accessories) {
        this.accessories = accessories;
    }

    public boolean isReservation() {
        return reservation;
    }

    public void setReservation(boolean reservation) {
        this.reservation = reservation;
    }
}
