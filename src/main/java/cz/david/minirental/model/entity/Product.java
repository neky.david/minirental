package cz.david.minirental.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.david.minirental.model.enums.ProductCategory;
import cz.david.minirental.helpers.ImageHelper;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.jsoup.Jsoup;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "product")
public class Product extends BaseEntity {

    @NotNull
    private String name;

    //@Lob
    @Column(name = "html", columnDefinition = "TEXT")
    @Length(max = 65535)
    @JsonIgnore
    private String html;

    @Column(name = "description", columnDefinition = "TEXT")
    @Length(max = 65535)
    private String description;

    @NotNull
    private double price;

    //Kauce
    private double bail;

    private ProductCategory category;

    private String mainImageName;

    @OneToMany(targetEntity = Image.class, cascade = CascadeType.ALL)
    @Valid
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    //@JoinTable(name = "product_images")
    private List<Image> images;

    @OneToMany(targetEntity = Accessory.class, cascade = CascadeType.ALL)
    @Valid
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    //@JoinTable(name = "product_accessories")
    private List<Accessory> accessories;

    @OneToMany(targetEntity = Borrow.class, mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    //@LazyCollection(LazyCollectionOption.TRUE)
    @Valid
    @JsonIgnore
    //@JoinTable(name = "product_borrowlist")
    private List<Borrow> borrows;

    public Product() {
        /*this.accessories = new ArrayList<>();
        this.accessories.add(new Accessory());*/
    }

    @JsonIgnore
    public Image getMainImage() {
        if(images != null && images.size() > 0) {
            for (Image img : images) {
                if(img.getName().equals(getMainImageName()))
                    return img;
            }
        }
        return null;
    }

    @JsonIgnore
    public List<Accessory> getAvailableAccessories(){
        List<Accessory> result = new ArrayList<>();
        for (Accessory a: accessories) {
            if(a.isOffer())
                result.add(a);
        }
        return result;
    }

    public long getRoundPrice(){
        return Math.round(price);
    }

    public String getNonHtml(){
        return html != null ? Jsoup.parse(html).text() : "";
    }

    public void setImages(MultipartHttpServletRequest request) throws IOException {
        images = ImageHelper.saveMultipartImage(request);
    }

    /**vrátí první datum kdy je produkt volný*/
    /*public Calendar getFirstAvailableDate(){
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, 1);
        //všechny výpůjčky po dnešku
        List<Borrow> list = borrows.stream()
                .filter(b -> b.getFromDate().after(yesterday))
                .collect(Collectors.toList());

        return Borrow.getFirstAvailableDate(list);
    }

    public static List<String> getStringsAvailableDates(List<Product> products){
        List<String> dates = new ArrayList<>();

        if(products != null && products.size() > 0){
            for (Product product :
                    products) {
                dates.add(CalendarHelper.getFormatString(product.getFirstAvailableDate(), false));
            }
        }

        return dates;
    }*/

    public String getShortDesc(){
        return getShortDesc(50);
    }

    public String getShortDesc(int charCount){
        return (description.length() > charCount) ? description.substring(0,charCount) + "..." : description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getBail() {
        return bail;
    }

    public void setBail(double bail) {
        this.bail = bail;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public String getMainImageName() {
        return mainImageName;
    }

    public void setMainImageName(String mainImageName) {
        this.mainImageName = mainImageName;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<Accessory> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Accessory> accessories) {
        this.accessories = accessories;
    }

    public List<Borrow> getBorrows() {
        return borrows;
    }

    public void setBorrows(List<Borrow> borrows) {
        this.borrows = borrows;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public void removeImages() {
        if(images != null){
            for (Image img : images) {
                img.removeImageFiles();
            }
        }
    }
}
