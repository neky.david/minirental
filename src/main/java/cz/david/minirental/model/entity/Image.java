package cz.david.minirental.model.entity;

import cz.david.minirental.helpers.ImageHelper;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "image")
public class Image extends BaseEntity {

    private String name;

    private String path;

    private int width;

    private int height;

    public Image() {
    }

    public Image(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getSmallName() {
        return name.substring(0, name.lastIndexOf(".")) + ".small.jpg";
    }

    public String getSmallPath() {
        if(path.contains("\\")){
            return path.substring(0, path.lastIndexOf("\\")) + "\\" + getSmallName();
        }
        else if(path.contains("/")){
            return path.substring(0, path.lastIndexOf("/")) + "/" + getSmallName();
        }
        return "";
    }

    public String getThumbnailName() {
        return name.substring(0, name.lastIndexOf(".")) + ".thumb.jpg";
    }

    public String getThumbnailPath() {
        if(path.contains("\\")){
            return path.substring(0, path.lastIndexOf("\\")) + "\\" + getThumbnailName();
        }
        else if(path.contains("/")){
            return path.substring(0, path.lastIndexOf("/")) + "/" + getThumbnailName();
        }
        return "";
    }

    public void removeImageFiles() {
        ImageHelper.deleteImage(path);
        String smPath = getSmallPath();
        ImageHelper.deleteImage(smPath);
        String tmbPath = getThumbnailPath();
        ImageHelper.deleteImage(tmbPath);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
