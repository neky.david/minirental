package cz.david.minirental.repository;

import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

public interface BorrowRepository extends JpaRepository<Borrow, Long>, JpaSpecificationExecutor<Borrow> {

    Optional<Borrow> getByIdAndReservation(Long id, boolean reservation);

    Page<Borrow> getByReservationEqualsAndActiveEquals(boolean reservation, boolean active, Pageable pageable);

    List<Borrow> getByProductOrderByFromDate(Product product);

    List<Borrow> getByProductAndFromDateAfterAndActiveOrderByFromDate(Product product, Calendar calendar, boolean active);

    List<Borrow> getByUser(User user);

    List<Borrow> getByUserAndFromDateAfterOrderByFromDate(User user, Calendar calendar);

    List<Borrow> getByUserAndFromDateBeforeOrderByFromDate(User user, Calendar calendar);

}
