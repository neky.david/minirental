package cz.david.minirental.repository;

import cz.david.minirental.model.entity.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Long> {
        Optional<Role> findByName(String name);
}