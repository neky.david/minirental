package cz.david.minirental.services;

import cz.david.minirental.model.entity.Accessory;
import cz.david.minirental.repository.AccessoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AccessoryService implements IBaseService<Accessory>  {
    private final AccessoryRepository repository;

    @Autowired
    public AccessoryService(AccessoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(Accessory a) {
        repository.save(a);
    }

    @Override
    public void update(Accessory a) {
        repository.save(a);
    }

    @Override
    public List<Accessory> getAll() {
        return (List<Accessory>)repository.findAll();
    }

    @Override
    public Optional<Accessory> getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public void remove(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void remove(Accessory a) {
        repository.delete(a);
    }
}
