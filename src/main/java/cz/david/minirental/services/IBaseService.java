package cz.david.minirental.services;

import java.util.List;
import java.util.Optional;

public interface IBaseService<T> {

    public void save(T obj);
    public void update(T obj);
    public List<T> getAll();
    public Optional<T> getById(Long id);
    public void remove(Long id);
    public void remove(T obj);
}

