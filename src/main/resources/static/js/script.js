/**GENERAL**/
jQuery(document).ready(function () {

    feather.replace();

    //tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = $('.needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
});


// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("btn-up").style.opacity = 100;
        document.getElementById("btn-up").style.right = '30px';
    } else {
        document.getElementById("btn-up").style.opacity = 0;
        document.getElementById("btn-up").style.right = '-30px';
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

function redirectTo(url) {
    window.location = url;
}

function redirectToPage(pageNumber) {
    $('#filterPageNumber').val(pageNumber);
    $('form#filterForm').submit();
}


function confirmWithData(text, btn) {
    try {
        if (text && btn) {
            var index = 0;

            while (text.includes("{" + index + "}")) {
                var data = btn.getAttribute('data-conf-' + index);
                if (data) {
                    text = text.replace("{" + index + "}", data);
                }
                else{
                    text = text.replace("{" + index + "}", "");
                }
                index++;
            }

            return confirm(text);
        }
        else {
            console.error("chyba ve funkci confirmWithData - text nebo event nejsou definovány")
        }

        return false;
    }
    catch (e) {
        console.error(e.toString());
        return false;
    }
}

/**LOGIN DROPDOWN**/

/*document.addEventListener("DOMContentLoaded", function(event) {
  //document.getElementById("login-form").style.display = "none";
});*/

function showLogin() {
    var loginForm = document.getElementById("login-form");
    if (loginForm.style.display === "none") {
        loginForm.style.display = "block";
    } else {
        loginForm.style.display = "none";
    }
}

jQuery(document).ready(function () {
    if(document.getElementById("login-form") != null){
        window.onclick = function (event) {
            if (event.target.matches('#btn-login') || ($(event.target).closest("#btn-login").length > 0)) {
                showLogin();
            } else if (!event.target.matches('#login-form')) {
                document.getElementById("login-form").style.display = "none";
            }
            //vybrání obrázku
            var prevElem = $(event.target).parents('.dz-preview');
            if (prevElem.length) {
                changeActiveImage(prevElem[0]);
            }
        };
    }
});

/**REGISTRATION**/

function checkPassword(pass, repass, required) {

    if (required) {
        if (pass.val().length < 8) {
            pass[0].setCustomValidity('invalid');
        } else {
            pass[0].setCustomValidity('');
        }
    } else {
        if (pass.val().length > 0 && pass.val().length < 8) {
            pass[0].setCustomValidity('invalid');
        } else {
            pass[0].setCustomValidity('');
        }
    }

    comparePasswords(pass, repass);
}

function comparePasswords(pass, repass, required) {

    if (required) {
        if (repass.val().length < 8 || pass.val() !== repass.val()) {
            repass[0].setCustomValidity('invalid');
        } else {
            repass[0].setCustomValidity('');
        }
    } else {
        if ((pass.val().length > 0 && repass.val().length < 8) || pass.val() !== repass.val()) {
            repass[0].setCustomValidity('invalid');
        } else {
            repass[0].setCustomValidity('');
        }
    }
}

function validateEmail(email) {
    var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    if (!email_regex.test(email.val())) {
        email[0].setCustomValidity('invalid');
    } else {
        email[0].setCustomValidity('');
    }

}


/** SPINNER **/
function showSpinner(title, message) {
    var modal = $('#spinner-modal');

    if (modal.length > 0) {
        document.getElementById('spinner-title').innerHTML = title;
        document.getElementById('spinner-message').innerHTML = message;
        //$('#spinner-title').innerHTML = title;
        //$('#spinner-message').innerHTML = message;
        modal.modal('show');
    }
    else {
        console.error("funkce showSpinner lze volat pouze pokud je nalinkovaný fragment spinner");
    }
}

function hideSpinner() {
    var modal = $('#spinner-modal');

    if (modal.length > 0) {
        document.getElementById('spinner-title').innerHTML = "";
        document.getElementById('spinner-message').innerHTML = "";
        //$('#spinner-title').innerHTML = "";
        //$('#spinner-message').innerHTML = "";
        modal.modal('hide');
    }
    else {
        console.error("funkce hideSpinner lze volat pouze pokud je nalinkovaný fragment spinner");
    }
}

/**DROPZONE**/

function addFileToDropZone(myDropzone, file, thumbnailUrl) {
    //var mockFile = { name: "eb78dbc8-5521-428f-9495-39caa64518f5.jpg", size: 13245};
    myDropzone.options.addedfile.call(myDropzone, file);
    myDropzone.options.thumbnail.call(myDropzone, file, thumbnailUrl);
}

/* document.getElementById('product-form').onsubmit = function(){
     var form = document.getElementById('product-form');
     console.log('aa');
 };*/

function changeActiveImage(elem) {
    var items = $('.dz-preview');
    items.removeClass("active");
    $(elem).addClass("active");
    var index = items.index(elem);
    $("#hdnImgIndex").val(index);
}

function getActiveImgIndex() {
    var index = -1;

    var allItems = $('.dz-preview');
    if (allItems != null && allItems.length > 0) {
        var activeItems = $('.dz-preview.active');
        if (activeItems != null && activeItems.length > 0) {
            index = allItems.index(activeItems[0]);
        }
    }
    return index;
}

function formSubmit(myDropzone, url) {
//validation
    if (document.getElementById('name').value === "") {
        document.getElementById('nameError').hidden = false;
        location.href = "#name";
    } else {
        document.getElementById('nameError').hidden = true;

        //CKEditor
        $('textarea.ckeditor').val(ckEditor.getData());

        var prevItems = $('.dz-preview').not('.saved');
        if (prevItems.length === 0) {
            showSpinner('Ukládání produktu', 'prosím čekejte, právě dáváme všechno dohromady.');

            var data = $("#product-form").find("input, select, textarea").serialize();

            //CKEditor
            /*var ckData = ckEditor.getData();
            if(ckData != null && ckData !== ""){
                data += "&html=" + ckData;
            }*/

            $.post(url, data, deserAndResponse);
        } else {
            showSpinner('Ukládání produktu', 'prosím čekejte, právě se nahrávají obrázky.');
            myDropzone.processQueue();
        }
    }
}

function deserAndResponse(response) {
    responseFunc(JSON.parse(response));
}

function responseFunc(response) {
    if (response.type === 'redirect') {
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host;
        var redirUrl = baseUrl + response.data;
        window.location.href = redirUrl;
    } else if (response.type === 'error') {
        document.getElementById("errorText").innerHTML = response.data;
        document.getElementById("errorMessage").hidden = false;
        location.href = "#main";
        document.getElementById('previews').innerHTML = "";
    }
}

/**CKEditor**/

var ckEditor;

function initCKEditor(selector) {
    ClassicEditor
        .create(document.querySelector(selector), {
            language: 'cs'
        })
        .then(newEditor => {ckEditor = newEditor;}).catch(error => {console.error(error);});
}


/**CLIPBOARD**/
function fallbackCopyTextToClipboard(text) {
    var textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        var successful = document.execCommand("copy");
        var msg = successful ? "successful" : "unsuccessful";
        console.log("Fallback: Copying text command was " + msg);
    } catch (err) {
        console.error("Fallback: Oops, unable to copy", err);
    }

    document.body.removeChild(textArea);
}

function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
    }
    navigator.clipboard.writeText(text).then(
        function () {
            console.log("Async: Copying to clipboard was successful!");
        },
        function (err) {
            console.error("Async: Could not copy text: ", err);
        }
    );
}

function copyInnerHtmlToClipboard(selector) {
    var elem = document.querySelector(selector);
    copyTextToClipboard(elem.innerHTML);
}

/**TOAST**/
function showToast(text) {
    // Get the snackbar DIV
    var x = document.getElementById("toast");
    x.innerText = text;

    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

/**BORROW NEW**/
function getDayName(dayCount) {
    if (dayCount === 1) {
        return "den";
    } else if (dayCount < 5) {
        return "dny"
    } else {
        return "dní"
    }
}

/**zobrazí alert s datumy v kolizi*/
function showCollisionDatesError(dates) {
    var message = "Ve vybraném datumu nelze vypůjčit produkt. V nasledujících datumech je již obsazený: ";

    dates.each(function (index) {
        var time = $(this).data("time");
        var date = moment(time);
        message += date.format('DD.MM.YYYY') + ", ";
    });

    message = message.substring(0, message.length - 2);

    var errorMessage = $('#borrowErrorMessage');
    errorMessage.html(message);
    var error = $('#borrowError');
    error.removeClass('d-none');
    error.addClass('show');

    location.href = "#borrow";
}

/**kontrola kolizí před submit*/
function checkCollisions() {
    var collisionDates = $('.lightpick__day.is-disabled.is-in-range')
    if (collisionDates.length > 0) {
        showCollisionDatesError(collisionDates);
        return false;
    }
    return true;
}

function closeAlert(selector) {
    var error = $(selector);
    error.removeClass('show');
    error.addClass('d-none');
}


/**USER AUTOCOMPLETE**/
function userAutocomplete(inp, hdnInp) {
    /*argumenty - input pro zobrazení jména, hidden input pro user id*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;

        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
            return false;
        }

        $.ajax({
            url: '/ajax/customer/autocomplete',
            type: 'POST',
            data: {value: val}, //JSON.stringify(orderId)
            success: function (data) {
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                inp.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < data.length; i++) {
                    var user = data[i];
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    b.classList.add("autocomplete-item");
                    b.innerHTML = "" +
                        "<div class=\"d-flex\">" +
                        " <div>\n" +
                        "  " + highlightText(user.name + " " + user.surname, val) + "\n" +
                        "  <p class=\"small text-muted mb-0\">" + highlightText(user.company, val) + "</p>\n" +
                        " </div>\n" +
                        " <div class=\"ml-auto text-right\">\n" +
                        "  <p class=\"small text-muted mb-0\">" + highlightText(user.email, val) + "</p>\n" +
                        "  <p class=\"small text-muted mb-0\">" + highlightText(user.phoneNumber, val) + "</p>\n" +
                        " </div>\n" +
                        " <input type=\"hidden\" class=\"userNameInput\" value=\"" + user.name + " " + user.surname + "\">\n" +
                        " <input type=\"hidden\" class=\"userIdInput\" value=\"" + user.id + "\">\n" +
                        "</div>";

                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByClassName("userNameInput")[0].value;
                        /*hidden input keep user id*/
                        hdnInp.value = this.getElementsByClassName("userIdInput")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            },
            error: function (a, b, c) {
                console.log(a, b, c);
            }
        });

    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        currentFocus = autocompleteInputKeydownFunc(e, currentFocus);
    });

    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target, inp);
    });
}

/**PRODUCT AUTOCOMPLETE**/
function productAutocomplete(inp, hdnInp) {
    /*argumenty - input pro zobrazení jména, hidden input pro user id*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;

        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
            return false;
        }

        $.ajax({
            url: '/ajax/product/autocomplete',
            type: 'POST',
            data: {value: val}, //JSON.stringify(orderId)
            success: function (data) {
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                inp.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < data.length; i++) {
                    var product = data[i];
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    b.classList.add("autocomplete-item");
                    b.innerHTML = "" +
                        "<div class=\"d-flex\">" +
                        " <div>\n" +
                        "  " + highlightText(product.name, val) + "\n" +
                        "  <p class=\"small text-muted mb-0\">" + highlightText(product.description, val) + "</p>\n" +
                        " </div>\n" +
                        " <div class=\"ml-auto text-right\">\n" +
                        "  <p class=\"small text-muted mb-0\">" + highlightText(product.id, val) + "</p>\n" +
                        " </div>\n" +
                        " <input type=\"hidden\" class=\"productNameInput\" value=\"" + product.name + "\">\n" +
                        " <input type=\"hidden\" class=\"productIdInput\" value=\"" + product.id + "\">\n" +
                        "</div>";

                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByClassName("productNameInput")[0].value;
                        /*hidden input keep user id*/
                        hdnInp.value = this.getElementsByClassName("productIdInput")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();

                        refreshBorrowProductPart(hdnInp.value);
                    });
                    a.appendChild(b);
                }
            },
            error: function (a, b, c) {
                console.log(a, b, c);
            }
        });

    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        currentFocus = autocompleteInputKeydownFunc(e, currentFocus);
    });

    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target, inp);
    });
}

function refreshBorrowProductPart(productId){
    $.ajax({
        url: '/admin/borrow/new/productpart',
        type: 'POST',
        data: {productId: productId}, //JSON.stringify(orderId)
        success: function (data) {
            $('#productPart').replaceWith(data);
            recolorNewBorrowSubmitBtns();
        },
        error: function (a, b, c) {
            console.log(a, b, c);
        }
    });
}

function recolorNewBorrowSubmitBtns(){
    var borrowBtn = $('#borrowBtn');
    var reservationBtn = $('#reservationBtn');
    var dateFrom = $("#fromDateInput");

    if(dateFrom.length > 0) {
        if (dateFrom[0].value === moment().format('DD.MM.YYYY')) {
            borrowBtn.removeClass('btn-outline-secondary').addClass('btn-success')
            reservationBtn.removeClass('btn-success').addClass('btn-outline-secondary')
        } else {
            reservationBtn.removeClass('btn-outline-secondary').addClass('btn-success')
            borrowBtn.removeClass('btn-success').addClass('btn-outline-secondary')
        }
    }
}

/**AUTOCMPLETE**/
function autocompleteInputKeydownFunc(e, currentFocus){
    var x = document.getElementById(e.target.id + "autocomplete-list");
    if (x) x = x.getElementsByClassName("autocomplete-item");
    if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        currentFocus = addActive(x, currentFocus);
    } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        currentFocus = addActive(x, currentFocus);
    } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
        }
        else {
            if (x) x[0].click();
        }
    }

    return currentFocus;
}

function addActive(x, currentFocus) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");

    return currentFocus;
}

function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
    }
}

function closeAllLists(elmnt, inp) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
        if(elmnt) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
        else {
            x[i].parentNode.removeChild(x[i]);
        }
    }
}

function highlightText(text, value){
    var reg = new RegExp(value.toString(), 'gi');
    var output = text.toString().replace(reg, function(str) {return '<b>'+str+'</b>'});
    return output;
}
